# Aplicacion Movil

A partir de la version [1.7.0.RELEASE](../about/release.md#version-170release "Version 1.7.0.RELEASE"), **MMS** ofrece a sus usuarios una aplicacion movil totalmente desarrollada en tecnologia Android, la cual esta totalmente integrada con la aplicacion web. En la misma se pueden realizar algunas de las funcionalidades que se hacen mediante la web.

La aplicacion esta compuesta por un menu del lado izquierdo en el cual se puede acceder a cualquier de las siguientes opciones:  

## Mi Perfil

Se visualiza informacion al perfil del usuario logueado en la aplicacion, a saber:

*	Nombre y apellido.
*	Sexo.
*	Identificacion.
*	Pais.
*	Fecha de creacion.

## Mi Empresa

Se observa una breve descripcion de la empresa, su idioma, la moneda utilizada, entre otra informacion basica.

## Listado de Ordenes de Trabajo

En esta pantalla se visualiza el listado de ordenes de trabajo en estado _Asignada, En desarrollo, o Pausada_ asignada al usuario logueado en la aplicacion. En cada una de ella se visualizan las acciones disponibles dependiendo del estado de cada orden de trabajo.

Estas acciones pueden ser:

*	Cargar horas de trabajo.
*	Pausar una orden de trabajo.
*	Cancelar una orden de trabajo.
*	Resolver una orden de trabajo.

## Calendario

El calendario es la pantalla mas importante de la aplicacion, donde en el mismo se visualizan todas las ordenes de trabajo mencionadas en el punto anterior (ver [Listado de Ordenes de Trabajo](/mobile/admin#listado-de-ordenes-de-trabajo "Listado de Ordenes de Trabajo")).

En el calendario se tiene dos acciones posibles:

*	Una de ellas es, al dejar presionado dos segundos sobre una orden de trabajo, se puede visualizar la informacion de dicha orden de trabajo.
*	Por otro lado, al presionar sobre una orden de trabajo aparecera un menu con las acciones disponibles en la misma. Esto depende del estado en que se encuentra cada orden de trabajo.
*	Finalmente, si se presiona sobre el calendario donde no se encuentra una orden de trabajo, le aparecera el popup de acciones disponibles, pero solamente con la opcion para crear una orden de trabajo (ver [Crear Orden de Trabajo](/mobile/admin#crear-orden-de-trabajo "Crear Orden de Trabajo")). 

## Crear Orden de Trabajo

En la aplicacion movil de **MMS**, el usuario tiene la posibilidad de crear una orden de trabajo, agregando imagenes tanto de la galeria de su telefono, o de la camara del mismo. En el formulario presentado en la aplicacion, se le pediran los siguientes datos:

*	Titulo de la orden de trabajo.
*	Descripcion de la orden de trabajo.
*	Listado de imagenes a agregar (maximo 3).

Una vez creada la orden de trabajo, se visualiza la informacion relacionada a la misma. Cabe aclarar que la misma no se visualizara en el listado de ordenes de trabajo, ni en el calendario, ya que la misma no se asigna automaticamente al usuario logueado a la aplicacion movil. La accion de asignacion de la orden de trabajo solo se puede realizar mediante la aplicacion web, y con los permisos suficientes.

## Configuracion

En esta pantalla el usuario puede configurar varios aspectos:

*	Activar/desactivar notificaciones de la aplicacion.
*	Cambiar el idioma de la aplicacion. Actualmente se encuentran los idiomas EspaNNol, e Ingles. Por defecto la aplicacion se visualiza en EspaNNol.
*	Desloguearse de la aplicacion.

## Desloguearse

Aqui, usted podra desloguearse de **MMS**, volviendo a la pantalla de ingreso de usuario y contraseNNa. 